package com.freecharge.emp.controller;

import com.freecharge.emp.exception.EmployeeAlreadyExistsException;
import com.freecharge.emp.exception.EmployeeNotFoundException;
import com.freecharge.emp.model.Employee;
import com.freecharge.emp.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("/")
    public String getHomePage(ModelMap model){
        model.addAttribute("empList", this.employeeRepository.getEmpList());
        return "index";
    }
   /* @PostMapping("/addemployee")
    public String addEmployee(@ModelAttribute("employee") Employee employee, ModelMap model) throws EmployeeAlreadyExistsException {
        this.employeeRepository.addEmployee(employee);
        model.addAttribute("empList", this.employeeRepository.getEmpList());
        return "index";
    }*/

    @PostMapping("/addemployee")
    public ModelAndView addEmployee(@ModelAttribute("employee") Employee employee) throws EmployeeAlreadyExistsException {
        this.employeeRepository.addEmployee(employee);
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("empList", this.employeeRepository.getEmployeeList());
        return modelAndView;
    }

    @GetMapping("/deleteemployee")
    public ModelAndView deleteEmployee(@RequestParam("empId") int id) throws EmployeeNotFoundException {
        this.employeeRepository.deleteEmployee(id);
        ModelAndView modelAndView = new ModelAndView("redirect:/");
        modelAndView.addObject("empList", this.employeeRepository.getEmployeeList());
        return modelAndView;
    }

}
