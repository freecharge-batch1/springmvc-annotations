package com.freecharge.emp.exception;

public class EmployeeAlreadyExistsException extends Exception{

    public EmployeeAlreadyExistsException(String message) {
        super(message);
    }
}
