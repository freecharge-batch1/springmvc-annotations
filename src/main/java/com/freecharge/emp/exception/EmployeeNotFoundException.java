package com.freecharge.emp.exception;

public class EmployeeNotFoundException extends  Exception{

    public EmployeeNotFoundException(String message) {
        super(message);
    }
}
