package com.freecharge.emp.repository;

import com.freecharge.emp.exception.EmployeeAlreadyExistsException;
import com.freecharge.emp.exception.EmployeeNotFoundException;
import com.freecharge.emp.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class EmployeeRepository {

    List<Employee> empList;

    public EmployeeRepository(){
        this.empList = new ArrayList<>();
    }

    public List<Employee> getEmpList(){
        return this.empList;
    }

    public Employee addEmployee(Employee employee) throws EmployeeAlreadyExistsException {
        Optional<Employee> empById = this.empList.stream().filter(emp -> emp.getId() == employee.getId()).findFirst();
        if(empById.isPresent())
            throw new EmployeeAlreadyExistsException("employee already exists with this id:" + employee.getId());
        this.empList.add(employee);
        return employee;

    }

    public List<Employee> getEmployeeList(){
        return this.empList;
    }


    public boolean deleteEmployee(int id) throws EmployeeNotFoundException {
        //busines logic to remove
        Optional<Employee> empById = this.empList.stream().filter(emp -> emp.getId() == id).findFirst();
        if(!empById.isPresent())
            throw new EmployeeNotFoundException("employee not found with this id :" + id);
        this.empList.removeIf(emp -> emp.getId() == id);
        return true;
    }
}
